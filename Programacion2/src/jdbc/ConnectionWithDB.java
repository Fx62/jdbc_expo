package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;

public class ConnectionWithDB {

	private Connection connect = null;
	private Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet resultSet = null;

	public String[][] readDB() throws Exception {
		String[][] registers = null;
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			String username = "ruzo";
			String password = "h4ppYh4ppY";
			String url = "jdbc:mariadb://192.168.1.5/cementerio";
			connect = DriverManager.getConnection(url, username, password);
			statement = connect.createStatement();
			resultSet = statement.executeQuery("select count(*) from muertos");
			if(resultSet.next()) {
				int i = 0;
				int j = 0;
				registers = new String[resultSet.getInt(1)][3];
				resultSet = statement.executeQuery("select * from muertos");
				while (resultSet.next()) {
					registers[i][j] = resultSet.getString(2);
					j++;
					registers[i][j] = String.valueOf(resultSet.getDate(3));
					j++;
					registers[i][j] = String.valueOf(resultSet.getDate(4));
					j++;
					j = 0;
					i++;
				}
			}
			connect.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return registers;
	}

	public void writeDB(Registers register) {
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			String username = "ruzo";
			String password = "S0p0rt3.";
			String url = "jdbc:mariadb://192.168.1.5/cementerio";
			connect = DriverManager.getConnection(url, username, password);
			statement = connect.createStatement();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			String query = "insert into muertos (`Nombre completo`, Nacimiento, Fallecimiento) values (\""
					+ register.getName() + "\", \"" + format.format(register.getBorn()) + "\", \""
					+ format.format(register.getDied()) + "\")";
			//System.out.println(query);
			preparedStatement = connect.prepareStatement(query);
			preparedStatement.executeQuery();
			connect.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
