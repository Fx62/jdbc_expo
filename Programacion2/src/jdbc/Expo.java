package jdbc;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

public class Expo {


	static ConnectionWithDB connection = new ConnectionWithDB();
	
	public static void main(String[] args) {
		
		// it is necesary to change filled() with the input user from gui
		filled();
		
		try {
			String[][] show = connection.readDB();
			System.out.println(show.length + "   " + show[0].length);
			for (int i = 0; i < show.length; i++) {
				for(int j = 0; j < show[i].length; j++) {
					System.out.print(show[i][j] + " ");
				}
				System.out.println();
			}
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	
	// this method is to write datas to db like a test
	public static void filled(){
		Registers register = new Registers();
		Date date = null;
		register.setName("Ana Frank");
		date = new GregorianCalendar(1929, 06, 12).getTime();
		register.setBorn(date);
		date = new GregorianCalendar(1945, 02, 04).getTime();
		register.setDied(date);
		connection.writeDB(register);
		
		register.setName("Miguel Angel Asturias");
		date = new GregorianCalendar(1899, 10, 19).getTime();
		register.setBorn(date);
		date = new GregorianCalendar(1974, 06, 9).getTime();
		register.setDied(date);
		connection.writeDB(register);
	}
}
